var config = {
    dprBaseUrl: '/distributionpartnerreport/secure/rest'
};

var getReportDetails = function getReportDetails(req){
    return parseInt(req.params.reportId);
}


/**
 * @return {string}     name of file
 */
var getReportsFileName = function getReportsFileName (req) {
    //Todo: Add additional logic to add smarts
    return 'reports-search';
}

/**
 * Return true if the call should return a result list
 *
 * @param req - the request
 * @param routeObj - the route object handling the request
 * @param persona - the user name
 * @returns {boolean} if the call should return a result list
 */
var isMultiResultSearch = function isMultiResultSearch(req, routeObj, persona) {
    return true;
};

module.exports = {
    get: {
        aggregators: {
            route: config.dprBaseUrl + '/aggregators',
            folder: 'aggregators',
            delay: 1500
        },
        metaData: {
            route: config.dprBaseUrl + '/reports/metadata',
            folder: 'metaData',
            delay: 400
        },
        reports: {
            route: config.dprBaseUrl + '/reports',
            folder: 'reportSearch',
            fileName: 'reports'
        },
        reportSearch: {
            route: config.dprBaseUrl + '/reports/search',
            isDataTablesData: isMultiResultSearch,
            // Properties needed in the case that 'isMultiResultSearch' returns true
            dataFolder: 'reportSearch',
            defaultPageSize: 10,
            // Properties needed in the case that 'isMultiResultSearch' returns false
            folder: 'reportSearch',
            fileName: getReportsFileName,
	        delay: 1500
        },
        reportDetails: {
            route: config.dprBaseUrl + '/reports/:reportId',
            folder: 'reportDetails',
            fileName: getReportDetails,
            delay: 1000
        },
        reportReview: {
            route: config.dprBaseUrl + '/reports/:reportId/review',
            folder: 'reportsReview',
            fileName : function(req) { return req.params.reportId; },
            delay: 1000
        },        
        employeeApplications: {
            route: config.dprBaseUrl + '/employeeapplications',
            folder: 'employeeApplications'
        },
        name: {
            route    : config.dprBaseUrl + '/reports/name/:name',
            folder   : 'name',
            fileName : function(req) { return req.params.name; }
        },
        user: {
            route: config.dprBaseUrl + '/user',
            folder: 'user'
        },
        partners: {
            route: config.dprBaseUrl + '/partners/:id',
            folder: 'partners',
            fileName : function(req) { var partnerType = req.query.partnerType;  return partnerType.replace("/", "") + '-' + req.params.id; },
            delay: 1000
        },   
        plans: {
            route    : config.dprBaseUrl + '/plans/:plan',
            folder   : 'plans',
            fileName : function(req) { return req.params.plan; }
        }
    },
    put : {
        reportDetails : {
            route: config.dprBaseUrl + '/reports/:reportId',
            folder: 'reportDetails'
        },
        status : {
            route: config.dprBaseUrl + '/reports/:reportId/status',
            folder: 'status'
        }
    },
    post : {
        createReport : {
            route: config.dprBaseUrl + '/reports',
            folder: 'reportDetails'
        },
        reportSearch: {
            route: config.dprBaseUrl + '/reports/search',
            isDataTablesData: isMultiResultSearch,
            // Properties needed in the case that 'isMultiResultSearch' returns true
            dataFolder: 'reportSearch',
            defaultPageSize: 10,
            // Properties needed in the case that 'isMultiResultSearch' returns false
            folder: 'reportSearch',
            fileName: '',
            delay: 1500
        }
    } 
};
