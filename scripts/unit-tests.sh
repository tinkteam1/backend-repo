#!/bin/bash

echo '--------------------------------------------------'
echo '------------- UNIT TESTS BEGINNING ---------------'
echo '--------------------------------------------------'
echo '\n'

script=./scripts/preserve-reports.sh
config=test/config/intern-unit

# move existing reports
# source $script PREV

# Must be run from top level of project directory
echo 'Running unit tests using' $config
./node_modules/.bin/intern-client config=$config

# rename our reports
# source $script unit
# move existing reports back
# source $script PREV invert

echo '--------------------------------------------------'
echo '--------------- UNIT TESTS ENDING ----------------'
echo '--------------------------------------------------'
echo '\n'

