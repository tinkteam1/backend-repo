/*jshint node: true */

// include a mappings.js file from each app you will support
// file should be formatted as below
//
// See the README file for details on the options for each route.
//
// var config = {
//     baseUrl: '/some/base/url'
// };
//
// module.exports = {
//     // DELETEs
//     delete: {
//         someSetOfRoutes: {
//             route: config.baseUrl + '/some/path/here',
//             folder: 'folder_to_use_for_data',
//             // ms to delay before response,
//             delay: 500
//             // if present, will fail the request, and set filename to return
//             fail: 500
//         }
//     },
//     // GETs
//     get: {
//         someSetOfRoutes: {
//             route: config.baseUrl + '/some/path/here',
//             folder: 'folder_to_use_for_data',
//             // can define a function to return the exact filename to use
//             fileName: function (req) {
//                 var foo = 'foo';
//                 return foo;
//             }
//         }
//     },
//     // POSTS
//     post: {
//         someSetOfRoutes: {
//             route: config.baseUrl + '/some/path/here',
//             folder: 'folder_to_use_for_data'
//         }
//     }
// }
//
// TODO
// handle route conflicts
// put in prefixes for each app?
//

var asMappings   	  = require('./accountservicesPrj/mappings');
var cdscheckMappings  = require('./cdscheckPrj/mappings');
var cscMappings       = require('./careSolutionsCalculator/mappings');
var ebenMappings      = require('./ebenPrj/mappings');
var eoiMappings       = require('./eoi/mappings');
var dprMappings       = require('./dprPrj/mappings');
var osoMappings       = require('./osoPrj/mappings');
var regMappings       = require('./registrationPrj/mappings');
var wcmMappings       = require('./wcm/mappings');

module.exports = {
    accountServicesPrj      : asMappings,
    careSolutionsCalculator : cscMappings,
    cdscheckPrj				: cdscheckMappings,
    ebenPrj                 : ebenMappings,
    dprPrj	                : dprMappings,
    eoi                     : eoiMappings,
    osoPrj                  : osoMappings,
    registrationPrj         : regMappings,
    wcm                     : wcmMappings
};
