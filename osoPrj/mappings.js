var config = {
    osoBaseUrl: '/api/oso/secure/rest',
    osoVintageBaseUrl: '/oso/secure/rest',
};


/**
 * Return true if the call should return a result list
 * 
 * @param req - the request
 * @param routeObj - the route object handling the request
 * @param persona - the user name
 * @returns {boolean} if the call should return a result list
 */
var isMultiResultSearch = function isMultiResultSearch(req, routeObj, persona) {
    if (req.query.caseId || 
            req.query.policyNumber || 
            (req.query.customerName && 
            (req.query.customerName === 'mouse' || req.query.customerName === 'no'))) {
        // if there's a caseId query string param, then it's a policy detail request
        // if policyNumber, then it's a policyNumber search (multi, but not pageable)
        // if customerName, then it's a customerName search
        return false;
    }
    return true;
};


/**
 * The request must have contained a "caseId" query string param, so extract that param's value
 * to build the filename.
 *
 * @param req - the request
 * @param routeObj - the route object handling the request
 * @param persona - the user name
 * @returns {string} The name of the file containing the policy data
 */
var getPolicyFileName = function getPolicyFileName(req, routeObj, persona) {
    if (req.query.caseId) {
        return 'awd-' + req.query.caseId;
    } else if (req.query.policyNumber) {
        return req.query.policyNumber;
    } else if (req.query.customerName && 
        (req.query.customerName === 'mouse' || req.query.customerName === 'no')) {
        return req.query.customerName;
    }
};

var getPolicyFolder = function getPolidyFolder(req, routeObj, persona) {
    if (req.query.caseId) {
        return 'policyDetail';

    } else if (req.query.policyNumber || req.query.customerName) {
        return 'pendingPolicySearch';
    }
};


var getOrgsFilename = function getOrgsFilename (req) {

    if (req.params && req.params.fileName) {
        return  req.params.fileName
    } 
    
    return 'direct-reports';
};

var getUserFilename = function getUserFilename(req, routeObj, persona) {
    if (req.query.targetuser) {
        return req.query.targetuser;
    } else {
        return null;
    }
};

/**
 * Get file name for policy summary list
 * if req.query.hierarchy = ORG -> return org-policies
 * if req.query.hierarchy = self/producer -> return -policies    
 *  
 * @param  {object} req request params
 * @return {string}     name of file
 */
var getPolicySummariesFileName = function getPolicySummariesFileName (req) {
    if (req.query.hierarchy && req.query.hierarchy === 'org') {
        return 'org-policies';
    }

    return 'policies';
}

/**
 * Get file name for producer search
 * @param  {object} req request params
 * @return {string}     
 */
var getProducerSearchFileName = function getProducerSearchFileName (req) {
    if (req.query.roleCode) {
        return 'rolecode';
    }

    return '';
}

module.exports = {
    get: {
        helloWorld: {
            route: config.osoBaseUrl + '/greeting',
            folder: 'hello'
            // generateStatus: true

            // Add the 'fail' prop and a status code.
            // Will use the .json file for the code (if it exists), or general error.
            // fail: 503
        },

        // Ideally, we would have a file function to use the producerId param as the filename.
        // But I don't think we really need to go that far for this.
        paths: {
            route: config.osoBaseUrl + '/producers/:producerId/paths',
            folder: 'paths'

        },
        // Path for a policy client name search is /policies?customerName=:searchString
        // Path for a policy number search is /policies?policyNumber=:policyNumber
        // Path for a policy detail by caseId is "/policies?caseId=:caseId"
        // We can't map routes by querystring params, unfortunately, so both are handled here.
        pendingPolicySearch: {
            route: config.osoBaseUrl + '/policies',

            isDataTablesData: isMultiResultSearch,

            // Properties needed in the case that 'isMultiResultSearch' returns true
            dataFolder: 'pendingPolicySearch',
            defaultPageSize: 5,

            // Properties needed in the case that 'isMultiResultSearch' returns false
            folder: getPolicyFolder,
            fileName: getPolicyFileName
        },
        policySummaryList: {
            route: config.osoBaseUrl + '/policies/summaries',
            folder: 'policySummaryList',
            fileName: getPolicySummariesFileName,
            setHeader : {
                name  : 'Last-Modified',
                value : '2016-09-19T15:34:00'
            }
        },
        policySummaryListCounts: {
            route: config.osoBaseUrl + '/policies/summaries/counts',
            folder: 'policySummaryListCounts'
        },
        performanceCenter: {
            route: config.osoBaseUrl + '/performanceCenter',
            folder: 'performanceCenter'
        },
        performanceCenterVintage: {
            route: config.osoVintageBaseUrl + '/performanceCenter',
            folder: 'performanceCenter'
        },
        policyDetail: {
            route: config.osoBaseUrl + '/policies/:fileName',
            folder: 'policyDetail'
        },
        producerSearch : {
            route    : config.osoBaseUrl + '/producers',
            folder   : 'producerSearch',
            fileName : getProducerSearchFileName
        },
        orgs: {
            route: config.osoBaseUrl + '/orgs/:fileName*?',
            isDataTablesData: isMultiResultSearch,
            defaultPageSize: 5,
            dataFolder: 'orgs',
            folder: 'orgs',
            fileName: getOrgsFilename,
            getExtraFields: [ 'managingProducer', 'lastRefreshDate' ]

        },
        sso : {
            route  : config.osoBaseUrl + '/sso/*?/:fileName',
            folder : 'sso'
        },
        user: {
            route: config.osoBaseUrl + '/user',
            folder: 'user',
            fileName: getUserFilename
        },
        logout: {
            route: config.osoBaseUrl+'/auth/logout',
            folder: 'logout'
        }
    },

    post:{
        log: {
            route: config.osoBaseUrl + '/ui/log',
            folder: 'log'
        },
        upload: {
            route : config.osoBaseUrl + '/policies/*?/requirements',
            folder : 'requirementsSubmission'
        }
    }
};
