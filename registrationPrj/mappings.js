var config = {
    registrationUrl: '/public/rest/registration'
};

module.exports = {
    get: {
        securityQuestions: {
            route: config.registrationUrl + '/securityQuestions',
            folder: 'securityQuestions',
            generateStatus: true
        }
    },
    post: {
        registeredUser: {
            route: config.registrationUrl + '/registeredUser',
            folder: 'registeredUser',
            generateStatus: true
        },
        registerUser: {
            route: config.registrationUrl + '/registerUser',
            folder: 'registerUser',
            generateStatus: true
        }
    }
};
