var config = {
  baseUrl: '/oawcm/public/caresolutionscalculator',
  calculate: '/calculate.json',
  pdf: '/pdf.json'
};

function endpoint(suffix) {
  return {
    route: config['baseUrl'] + config[suffix],
    folder: suffix,
    noCookie: true
  }
}

function buildMapping(suffixes, map, suffix) {
  map = map || {};
  if(suffix)
    map[suffix] = endpoint(suffix);
  return 0 < suffixes.length ? buildMapping(suffixes, map, suffixes.pop()) : map;
}

module.exports = {
  post: buildMapping(['calculate', 'pdf'])
};
