var config = {
    baseUrl: '/cdscheck/secure/rest'
};

module.exports = {
    get: {

        user: {
            route: config.baseUrl + '/user',
            folder: 'user',
            delay: 1000
        },
        requests: {
            route: config.baseUrl + '/requests',
            folder: 'requests',
            delay: 1000
        },
		requestsDetails: {
            route: config.baseUrl + '/requests/:clientId',
            folder: 'requestsDetails',
            delay: 1000
        },
		alternateVoidAccounts:{
			route: config.baseUrl + '/metadata/alternateVoidAccounts',
            folder: 'metadata/alternateVoidAccounts',
            delay: 1000
		},
		companies: {
            route: config.baseUrl + '/metadata/companies',
            folder: 'metadata/companies',
            delay: 1000
        },	
		mailCodes: {
            route: config.baseUrl + '/metadata/mailCodes',
            folder: 'metadata/mailCodes',
            delay: 1000
        },
		reasons: {
            route: config.baseUrl + '/metadata/reasons',
            folder: 'metadata/reasons',
            delay: 1000
        },
		states: {
            route: config.baseUrl + '/metadata/states',
            folder: 'metadata/states',
            delay: 1000
        }
    },
    put : {
    },
    post : {
    } 
};
