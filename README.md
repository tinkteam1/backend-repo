# dev-localData

Providing emulation of REST APIs via local JSON data.

## How to use this repo

- Clone it locally.
- Run `npm install`
- Run `npm start`
- Visit `http://localhost:5000/YOUR/PATH`

Your app must set a persona cookie that maps to a set of data files or specify that noCookie is true on a per route basis.

You can set this cookie via an HTML page, or in the browser console after navigating to `http://localhost:5000`. To set in the console, enter `document.cookie = "persona=primary-user"`, for example.

Changes to any file in the repo will cause the server to restart itself.

## Route Mappings

Routes are defined (typically) in `mapping.js` files. The routes are grouped by HTTP verb. Each route mapping contains several components:

- **delay**: (Optional) Use the delay value before sending the response (otherwise a random delay is inserted)
- **fail**: (Optional) A function that returns an object like this:
`
{
code: 401,
message: 'Fail message',
fileName: 'fail.json'
}
`
The `fileName` property is the name only (not extension) of a file located in the `folder` defined for that route. It is expected to be a JSON file. If you define `fileName`, the contents of that file will be used instead of the message property. `message` can be used to send back only a simple string.

- **folder**: The folder containing the mock JSON responses
- **generateStatus**: (Optional) Whether to automatically select between 200 and 400 status reponses based on the data
- **fileName**: (Optional) Use the specified filename or function (rather than than building a name from the verb and the persona)
- **noCookie**: (Options, boolean) Is a 'persona' cookie needed to access this route. If set to `true`, will bypass any cookie check and continue through the rest of the code, returning a response as appropriate by the request and the processing of that request. In other words, this has no effect on what file SHOULD be returned, it just bypasses the checks and lets the rest of the app handle that.
- **route**: The URL to the route. These may use string wildcards or regular expressions, if you need matching. Note that the characters `*, ?, +, (, and )` are not the same in string wildcards as they are in regular expressions. See [Express routing documentation](http://expressjs.com/guide/routing.html) for more detail.
- **isHtml**: (Optional) Controls whether the content is returned as HTML
- **isBinary**: (Optional) Controls whether the content is returned as an binary file. Uses the file extension to determine the MIME type (application/jpeg when the file is foo.jpeg, for example)
- **isDataTablesData**: (Optional) A function to determine whether or not to use DataTables-style request params and response
- **dataFolder**: (Optional) If isDataTablesData() returns true, this specifies the folder for the data files
- **dataSrcName**: (Optional) If isDataTablesData() returns true, this specifies the 'dataSrc' property name in the response.

**IMPORTANT** The query string delimiter '?' and the query parameters are **NEVER** part of the route matching. Do **NOT** include those in your regex for the matching.

Example:

        {
            get: {
                securityQuestions: {
                    fileName: 'all.json',
                    folder: 'securityQuestions',
                    generateStatus: true,
                    noCookie: true,
                    route: config.baseUrl + '/registration/securityQuestions'
                }
            }
        }

## Development Mode Support

Various development settings are available to alter the behavior of the responses. These are triggered by setting
cookies in the browser that is using the mock data.

The settings are:

Name     | Description
---------|------------
dev      | If set to true, the mock responses are returned immediately. Otherwise, a random delay is inserted.
devError | If set to true, a HTTP 500 response will be returned (regardless of what the persona data would otherwise indicate).

## Example URLs

Current data in the app is borrowed from Account Services.

- http://localhost:5000/secure/rest/account
- http://localhost:5000/secure/rest/account/foobar

## How it works

Mostly like AS 2.0, but without redirecting you to any page to set a cookie.

This assumes your app will handle sending someone to the correct page to get the cookie. This app only works once that cookie is there.

A tool called [nodemon](https://github.com/remy/nodemon) is used to watch for changes in the files in the repo.

## Caveats

No detection of collisions between apps. If two apps define the same rest route, last one in wins.
