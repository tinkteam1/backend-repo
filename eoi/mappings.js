var config = {
    'baseUrl': '/eoiServices/secure/rest',
    'login': '/login',
    'authenticate': '/authenticate',
    'status': '/status',
    'personalInformation': '/personalInformation',
    'coverage': '/coverage',
    'questions': '/questions',
    'signature': '/signature',
    'fraud' : '/fraud'
};

function url(suffix) {
    return config['baseUrl'] + suffix;
}

module.exports = {
    get: {
        'status': {
            route: url(config['status']),
            folder: 'status'
        },
        'personalInformation': {
            route: url(config['personalInformation']),
            folder: 'personalInformation'
        },
        'coverage': {
            route: url(config['coverage']),
            folder: 'coverage'
        },
        'questions/1': {
            route: url(config['questions'] + '/1'),
            folder: 'questions/1'
        },
        'questions/2': {
            route: url(config['questions'] + '/2'),
            folder: 'questions/2'
        },
        'questions/3': {
            route: url(config['questions'] + '/3'),
            folder: 'questions/3'
        },
        'questions/4': {
            route: url(config['questions'] + '/4'),
            folder: 'questions/4'
        },
		wcm_process_info: {
            route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Process-Info',
            folder: 'wcm_content',
            fileName: 'ProcessInfo',
            isHtml: true
        },
        wcm_welcome_info: {
            route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Home-Page-Intro',
            folder: 'wcm_content',
            fileName: 'WelcomeInfo',
            isHtml: true
        },
        wcm_process_status_header: {
            route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Process-Status-Header',
            folder: 'wcm_content',
            fileName: 'ProcessStatusHeader',
            isHtml: true
        },
		wcm_results_approved: {
			route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Results-Approved',
			folder: 'wcm_content',
			fileName: 'ResultsApproved',
			isHtml: true
		},
		wcm_results_declined: {
			route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Results-Declined',
			folder: 'wcm_content',
			fileName: 'ResultsDeclined',
			isHtml: true
		},
		wcm_results_invitation_sent: {
			route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Results-Invitation-Sent',
			folder: 'wcm_content',
			fileName: 'ResultsInvitationSent',
			isHtml: true
		},
		wcm_results_under_review: {
			route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Results-Under-Review',
			folder: 'wcm_content',
			fileName: 'ResultsUnderReview',
			isHtml: true
		},
		wcm_fraud_warning: {
			route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Fraud-And-Authorization',
			folder: 'wcm_content',
			fileName: 'FraudWarning',
			isHtml: true
		},
    wcm_contact_us: {
      route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/Contact-Us',
      folder: 'wcm_content',
			fileName: 'ContactUs',
			isHtml: true
    },
        'list_arrow_image': {
            isBinary: true,
            folder: 'wcm_content',
            fileName: 'ListArrowImage.png',
            route: '/wcm/wps/wcm/connect/eben/Enterprise-Forms/eoi/ListArrowImage'
        }
    },
	put: {
		'personalInformation': {
            route: url(config['personalInformation']),
            folder: 'personalInformation'
        },
        'fraud': {
            route: url(config['fraud']),
            folder: 'fraud'
        }
	},
    post: {
        'login': {
            route: url(config['login']),
            folder: 'login'
        },
        'authenticate': {
            route: url(config['authenticate']),
            folder: 'authenticate'
        },
        'personalInformation': {
            route: url(config['personalInformation']),
            folder: 'personalInformation'
        },
        'coverage': {
            route: url(config['coverage']),
            folder: 'coverage'
        },
        'questions/1': {
            route: url(config['questions'] + '/1'),
            folder: 'questions/1'
        },
        'questions/2': {
            route: url(config['questions'] + '/2'),
            folder: 'questions/2'
        },
        'questions/3': {
            route: url(config['questions'] + '/3'),
            folder: 'questions/3'
        },
        'signature': {
            route: url(config['signature']),
            folder: 'signature'
        }
    }
};
