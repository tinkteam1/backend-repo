#!/bin/bash
originalFile=$1
rootpath=${originalFile: : -8}
reasonIndex=0
while [ $reasonIndex -lt 4 ]; do
    progressIndex=0
    while [ $progressIndex -lt 5 ]; do
        statusIndex=1
        while [ $statusIndex -lt 6 ]; do
          decisionIndex=0
          while [ $decisionIndex -lt 5 ]; do
            cp "$rootpath$reasonIndex$progressIndex$statusIndex.json" "$rootpath$decisionIndex$reasonIndex$progressIndex$statusIndex.json"
            decisionIndex=$((decisionIndex + 1))
          done
            statusIndex=$((statusIndex + 1))
        done
        progressIndex=$((progressIndex + 1))
    done
    reasonIndex=$((reasonIndex + 1))
done
