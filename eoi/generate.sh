#!/bin/bash
originalFile=$1
rootpath=${originalFile: : -8}
reasonIndex=0
while [ $reasonIndex -lt 4 ]; do
    progressIndex=0
    while [ $progressIndex -lt 5 ]; do
        statusIndex=1
        while [ $statusIndex -lt 6 ]; do
            cp $originalFile "$rootpath$reasonIndex$progressIndex$statusIndex.json"
            statusIndex=$((statusIndex + 1))
        done
        progressIndex=$((progressIndex + 1))
    done
    reasonIndex=$((reasonIndex + 1))
done
