/*global console, __dirname, module, require, setTimeout */
/*jshint browser: true, node:true */

var fs   = require('fs');
var path = require('path');
var url  = require('url');
var util = require('util');

var pageableData = require('./localdataUtilities/pageableData');

// Don't ever cache when we are using local data
var defaultHeaders = {
    'Cache-Control':'private,no-cache,no-store,must-revalidate,max-age=0',
    "Surrogate-Control":"no-store",
    'Pragma':'no-cache'
};

/**
 * Generates a random integer between 0 and 2,750.
 * @returns {number}
 */
function generateRandomDelay() {
    return Math.round(Math.random() * 2.75 * 1000);
}

/**
 * give hash of cookies
 * @param req
 * @returns {.headers.cookie|*|.data.cookie|HTMLCollection|cookie|document.cookie}
 */
function parseCookies(req) {
    var cookies = req.headers.cookie || [];
    if (cookies.length > 0) {
        cookies = cookies.split(';');
        cookies.forEach(function (cookie) {
            var current = cookie.split('=');
            cookies[current[0].trim()] = current[1].trim();
        });
    }
    return cookies;
}

/**
 * determines which file to return based on configuration values
 *  and input received on the request.
 *
 * @param   {object}    routeObj
 * @param   {object}    persona
 * @param   {object}    req
 * @param   {object}    options                 Options used in the function to control behavior
 * @param   {string}    options.useNameProperty Should the routeObj.fileName property be used to create the filename?
 * useNameProperty
 * @returns {string}
 *
 */
function generateFileName(routeObj, persona, req, options) {

    console.log('generateFileName routeObj:' + JSON.stringify(routeObj) + " req:" + JSON.stringify(req.params));
    var defaults = {
        // Should the fileName property be used in creating the file name?
        // One time you might want to set this to false is when you
        // are using the 'fail' property to conditionally fail requests.
        // When you do that, you set the name of the 'failure' file as a child
        // property, and you may not want to use the 'success' fileName property
        // at all.
        useNameProperty: true
    };
    
    options = options || defaults;
    
    var file_name;
    console.log("routeObj.fileName:" + routeObj.fileName);
    // defined as a function?
    if (typeof routeObj.fileName === 'function') {
        // Note the arguments passed here
        // This is NOT the same order as above
        // But prevents a breaking change
        file_name = routeObj.fileName(req, routeObj, persona);

    } else if (req.params.fileName) {
        // Used for route definitions like '/stuff/:fileName'
        file_name = req.params.fileName;

    } else {
        file_name = (options.useNameProperty === true ? routeObj.fileName : null);
    }

    // If is binary, just bring back the file name and don't process further
    if (file_name && /(?:jpe?g|png|gif|mov|avi|swf|mpe?g)$/.test(file_name) === true) {
        return file_name;
    }

    // Is it defined at all?
    file_name = file_name || '';

    // Files get prefixed with the persona that they apply to
    if (file_name) {
        file_name = '-' + file_name;
    }
    file_name = persona + file_name;

    // Different HTTP verbs use different files
    if (req.method === 'POST') {
        file_name = 'post-' + file_name;
    }
    if (req.method === 'PUT') {
        file_name = 'put-' + file_name;
    }

    // What type of response?
    if (routeObj.isHtml) {
        file_name += '.html';
    } else {
        file_name += '.json';
    }
    console.log('file_name:' + file_name);
    return file_name;
}


/**
 * Determine the status code for the response.
 *
 * @param   {Object}    routeObj - The specification for a route from the mappings.js files
 * @param   {Object}    data
 *
 */
function getStatusCode(routeObj, data) {
    var responseCode;
    if (routeObj.generateStatus && !routeObj.isHtml) {
        var jsonData = JSON.parse(data);
         if (jsonData.items) {
            responseCode = 200;
         } else {
            responseCode = 400;
         }
    } else {
        responseCode = 200;
    }
    return responseCode;
}

/**
 * Return an error with an optional message.
 * @param   {Object}    res
 * @param   {Object}    err
 * @param   {Number}    [code]
 * @param   {Number}    [delay]
 */
function failJson(res, err, code, delay) {
    console.log('failJson:' + code);
    if (delay && typeof delay === 'number') {

        // fake a short delay for rest routes
        setTimeout(function () {
            failJson(res, err, code);
        }, delay);

    } else {

        res.writeHead(code || 500, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(err));
        res.end();
    }
}

/**
 * Node response which supports HTML and JSON
 * and allows for a delay to be specified.
 *
 * @param   {Object}    res
 * @param   {Object}    routeObj - The specification for a route from the mappings.js files
 * @param   {Object}    data
 * @param   {Number}    [delay]
 *
 */
function successResponse(res, routeObj, data, delay) {

    if (delay && typeof delay === 'number') {

        // fake a short delay for rest routes
        setTimeout(function () {
            successResponse(res, routeObj, data);
        }, delay);

    } else {

        var headers = defaultHeaders;

        // determine if additional headers need to be set
        if (routeObj.setHeader && routeObj.setHeader.name && routeObj.setHeader.value) {
            headers[routeObj.setHeader.name] = routeObj.setHeader.value;
        }

        if (routeObj.isHtml) {
            headers['Content-Type'] = 'text/html;charset=UTF-8';
        } else {
            headers['Content-Type'] = 'application/json;charset=UTF-8';
        }

        var responseCode = getStatusCode(routeObj, data);

        res.writeHead(responseCode, headers);

        if (routeObj.isHtml) {
            res.write(data);
        } else {
            res.write(JSON.stringify(JSON.parse(data)));
        }
        
        res.end();
    }
}

/**
 * Node response for binary data
 * and allows for a delay to be specified.
 *
 * @param   {Object}    res Node response object
 * @param   {Object}    binaryPath Path to the file we want to serve
 * @param   {Object}    binaryFile Filename
 *
 */
function binaryResponse(res, binaryPath, binaryFile) {

    // We will use the . separator to find the file extension
    var fileNameParts = binaryFile.split('.');

    if (fileNameParts.length === 0) {
        res.writeHead(500, 'Filename MUST have an extension specified.');
        res.end();
        return false;
    }

    // Check for a valid file
    if (binaryPath && binaryFile) {

        // Give a successful response with the binary data
        // Because of how serving binary data works, this has to complete the
        // request here, versus the normal successResponse method
        
        var binary = fs.readFile(path.join(binaryPath, binaryFile), function(err, data) {

            if (err) {
                res.writeHead(404);
                res.end();
                return false;
            }

            // The actual file exists
            var headers = defaultHeaders;
            headers['Content-Type'] = 'application/' + fileNameParts[fileNameParts.length - 1];
            res.writeHead(200, headers);
            res.end(data, 'binary');
            return true;
        });

    }

}

/**
 * default for /secure/rest routes
 * @param req
 * @param res
 * @param next
 * @param routeObj
 */
function handleXHR (req, res, next, routeObj) {
    debugger;
    var cookies;
    var delay;
    var file_name;
    var file_name2;
    var file_path;
    var error_file_name;

    // Log routes as they are requested.
    util.log(req.method + ': ' + req.url);

    // get any cookies present for parsing below
    cookies = parseCookies(req);

    if (!routeObj.noCookie) {

        if (cookies.length === 0 || !(cookies.persona)) {
            util.log('No cookies set for ', req.url);
            res.writeHead(401, 'No persona cookie was set');
            // This simulates the content that Siteminder returns with
            // a 401 response when the user is not logged in.
            res.write('SiteminderReason=Challenge\n' +
                'SiteminderRedirectURL=www.oneamerica.com\n' +
                'SiteminderChallengeURL=');
            res.end();
            return false;
        }

    } // cookie check

    // process based on the request
    if (typeof routeObj.folder === 'function') {

        //passing cookie 'appName' to refer the content is specific to application name
        routeObj.folder = routeObj.folder.call(this, req, cookies.appName);
    }

    if (typeof routeObj.filename === 'function') {
        routeObj.folder = routeObj.filename.call(this, req);
    }

    // setup the delay
    // unless in dev mode, put a random delay
    delay = 0;
    if (typeof routeObj.delay === 'number') {
        delay = routeObj.delay;
    } else if (!(cookies.dev && cookies.dev === 'true')) {
        delay = generateRandomDelay();
    }

    // If the cookie is set to force an error for all requests, return
    // a failure response
    if (cookies.devError) {
        var statusCode = isNaN(Number(cookies.devError)) ? 500 : cookies.devError;
        failJson(res, {message: 'Forced failure due to development mode setting'}, statusCode, delay);
        return true;
    }

    // If you define a function, you can conditionally fail the call
    // The function will be called with the arguments of req, routeObj, and persona
    if (routeObj.fail && typeof routeObj.fail === 'function') {
        routeObj.fail = routeObj.fail(req, routeObj, cookies.persona);
    }
    
    if (routeObj.fail && typeof routeObj.fail === 'object') {

        if (routeObj.fail.fileName) {

            // fail and return the designated file for failed data?
            error_file_name = generateFileName(routeObj, routeObj.fail.fileName, req, { useNameProperty: false });

            file_path = path.join(__dirname, routeObj.namespace, routeObj.folder, error_file_name);

            fs.readFile(file_path, { encoding: 'utf8' }, function (err, data) {

                if (err) {

                    // No fail file there, should get the node error from the first error
                    // This will be a Node error telling you it could not find the file
                    failJson(res, err);

                } else {

                    // file is there, return a defined error from the file
                    failJson(res, JSON.parse(data), routeObj.fail.code, delay);

                }
            });

        } else if (routeObj.fail.message) {

            // file is there, return a defined error from the file
            failJson(res, routeObj.fail.message, routeObj.fail.code, delay);
            
        }

    } else if (req.method === 'GET' && routeObj.isBinary === true) {

        file_path = path.join(__dirname, routeObj.namespace, routeObj.folder);

        file_name = generateFileName(routeObj, cookies.persona, req);

        binaryResponse(res, file_path, file_name);

    } else if (
        req.method === 'GET' && routeObj.isDataTablesData &&
        routeObj.isDataTablesData(req, routeObj, cookies.persona) === true
    ) {

        // generate data
        var data = pageableData.getDataTablesData(routeObj, cookies.persona, req);
        successResponse(res, routeObj, JSON.stringify(data), delay);

    } else if (req.method === 'DELETE') {

        // real technical here
        successResponse(res, routeObj, {message: 'ok' }, delay);

    } else {

        // give a successful response
        file_name = generateFileName(routeObj, cookies.persona, req);

        file_path = path.join(__dirname, routeObj.namespace, routeObj.folder);

        fs.readFile(path.join(file_path, file_name), { encoding: 'utf8' }, function (err, data) {

            // specific file is not there, fall back to an all.json file?
            if (err) {

                // look for an all.json file
                file_name2 = generateFileName(routeObj, 'all', req);

                // make sure we weren't asking for the all.json file in the first place
                if (file_name2 !== file_name) {

                    fs.readFile(path.join(file_path, file_name2), { encoding: 'utf8' }, function (err, data) {
                        if (err) {

                            // can't read an all.json file
                            // send the node error along
                            failJson(res, err, 404);

                        } else {

                            // respond with the all.json data
                            successResponse(res, routeObj, data, delay);

                        }
                    });

                } else {

                    // we were looking for the all.json file in the first place
                    // and it's not there
                    // send along the node error
                    failJson(res, err);

                }

            } else {

                // send along the specified response
                successResponse(res, routeObj, data, delay);

            }
            
        });
    }

    return true;
}

module.exports = {
    handleXHR: handleXHR
};
