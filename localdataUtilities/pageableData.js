/**
 * Utilities to return a subset of data stored in json files. The files should be in the following
 * form:
 *
 * {
 *     "data": [
 *         { "prop1":"val1", "prop2":"val2" },
 *         ... more records ...
 *         { "prop1":"val1", "prop2":"val2" }
 *     ]
 * }
 *
 */
var fs      = require('fs');
var path    = require('path');
var extend  = require('util')._extend;

/**
 * Sort the records on the specified column and direction.
 *
 * @param records    - the records to sort
 * @param sortColumn - the name of the data property to sort on
 * @param sortDir    - the direction of the sort (asc or desc)
 * @returns {Array} the sorted records array
 */
var sortData = function sortData(records, sortColumn, sortDir) {
    // Do a simple String-based sort on that column
    records.sort(function (a, b) {
        if (a[sortColumn]) {
            if (sortDir === 'asc') {
                return (String(a[sortColumn])).localeCompare(String(b[sortColumn]));
            } else {
                return (String(b[sortColumn])).localeCompare(String(a[sortColumn]));
            }
        }
    });

    return records;
};

/**
 * Slice the data to return a 'page' of data
 *
 * @param records - the records to slice
 * @param length  - the length of the data
 * @param start   - the 0-based start index of the data
 * @returns {Array} the sorted records array
 */
var sliceData = function sliceData(records, length, start) {
    var returnRecords = [];
    var end;

    // We have either a length param or a defaultPageSize prop, so slice the data
    if (length) {
        end = Number(start) + Number(length);
        if (start < records.length) {
            if (end >= records.length) {
                returnRecords = records.slice(start);
            } else {
                returnRecords = records.slice(start, end);
            }
        }
    }
    return returnRecords;
};

/**
 * Read the data from a .json file. First looks for a persona-named file in the routeObj.dataFolder
 * property and then falls back to an all-reports.json file in the same directory.
 *
 * @param routeObj - The routeObj defined in the mapping file. This function uses the 'namespace'
 *                   and 'dataFolder' properties of the the routeObj to locate the files.
 *
 * @param persona  - The user name.
 * @returns {Array} - an array of data read from the file or an empty array if no file exists.
 */
var readData = function readData(routeObj, persona) {
    var file;
    var data;
    var extraFields = {};
    var jsonData;
    var records = [];
    

    try {
        // read the data from a persona file
        file = path.join(__dirname, '..',  routeObj.namespace, routeObj.dataFolder, persona + '.json');
        data = fs.readFileSync(file, {encoding: 'utf8'});
        records = JSON.parse(data).data;

    } catch (error) {
        try {
            // no persona-named file. Try 'all-reports.json'.
            file = path.join(__dirname, '..', routeObj.namespace, routeObj.dataFolder, 'all.json');
            data = fs.readFileSync(file, {encoding: 'utf8'});
            jsonData = JSON.parse(data);
            
            if (jsonData) {
                if (jsonData.data) {
                    records = jsonData.data;
                }
                
                // if response needs additional fields other than data
                // just pass those field names with routeObj property "getExtraFields"
                // eg : getExtraFields : [ 'managingProducer', 'lastRefreshDate' ]
                if (routeObj.getExtraFields && Array.isArray(routeObj.getExtraFields)) {
                    routeObj.getExtraFields.forEach(function(val) {
                        if (jsonData[val] ) {
                            extraFields[val] = jsonData[val];
                        }
                    });
                }
            }

        } catch (error) {
            // some sort of status code here, perhaps?
            console.error('No data file ' + file);
        }
    }

    return {
        records     : records,
        extraField : extraFields
    }
};



/**
 * Return paged and/or sorted data for a jQuery.DataTables control.
 *
 * @param routeObj - the route object in the [project]/mappings.js file. It will use the following
 *                   properties on the route object:
 *
 *    "dataSrcName"     - the value to use for the 'dataSrc' property in the response, and the name
 *                        of the property that contains the actual data. Defaults to 'data'.
 *    "dataFolder"      - The folder (within the project) that contains the json files with data
 *    "defaultPageSize" - A default page size to use if the 'length' query string param is not
 *                        present. If neither the 'defaultPageSize' property or 'length' param
 *                        exist, the function will add ALL data to the response.
 *
 * @param persona  - the User Name
 *
 * @param req      - the Express HTTP Request object. The request may have the following query
 *                   string parameters to control sorting/paging:
 *
 *     "order[0][column]"   - index of the column to sort on (idx below)
 *     "order[0][dir]"      - direction of the sort (defaults to 'asc'
 *     "columns[idx][name]" - name of the column to sort on (idx above)
 *     "start"              - 0-based starting position for paging
 *     "length"             - number of records to return for page
 *     "draw"               - a DataTables param. If it exists, put it in the response.
 *
 *     Additionally, DataTables sends all column data, so in testing manually (not through DataTables),
 *     we need to add a columns[i][name] for each column in the table. We do not need to include
 *     other params, like "columms[i][search][regex]" at this time.
 *
 * @returns {} - a response to be used by DataTables in the following format:
 *
 * {
 *     recordsTotal: {integer},
 *     recordsFiltered: {integer},
 *     dataSrc: {string, name of below property},
 *     [value of dataSrc]: [ {Array of data} ]
 * }
 *
 */
var getDataTablesData = function getDataTablesData(routeObj, persona, req) {

    var response = {
        recordsTotal: 0,
        recordsFiltered: 0,
        dataSrc: routeObj.dataSrcName || 'data'
    };

    var length  = req.query.length || routeObj.defaultPageSize;
    var sortCol = (req.query.order) ? req.query.order[0][0] : 0;
    var start   = req.query.start || 0;

    // set the ordering props (will default to 0, 'asc')
    var sortColumn = req.query.columns[sortCol][0];
    var sortDir    = (req.query.order) ? req.query.order[0][1] : 'asc';

    var data = readData(routeObj, persona);
    var records = data.records;

    if (records) {
        // set the recordsTotal prop on the response
        response.recordsTotal = records.length;

        // set the recordsFiltered prop. We're not using filtering, but without this prop, paging fails.
        response.recordsFiltered = records.length;

        // Do a simple String-based sort on that column
        records = sortData(records, sortColumn, sortDir);

        // page the data
        records = sliceData(records, length, start);

        // set the records in the response
        response[response.dataSrc] = records;
    }

    // return the 'draw' param if it exists
    if (req.query.draw) {
        response.draw = req.query.draw;
    }

    // return the 'managingProducer' param if it exists
    if (data.extraField) {
        response = extend(response, data.extraField);
    }

    return response;
};

module.exports = {
    getDataTablesData: getDataTablesData
};
