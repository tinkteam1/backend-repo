# Local Data

## Adding account data

You must add a JSON file in the `accounts` folder and the `accountsummary` folder.
The `accounts` file should contain only the user and policy plan info.
The `accountsummary` file should contain all the account data.

## Current Mappings

### Retirement Services

- primary-user     :: Active Participant - No RIA On Plan
- primary-user-old :: Alternative Data Set
- account-selector :: Multiple Accounts
- fixed-fund-only  :: Fixed Fund Only

#### Active Participant In RIA

- ria-aplus-allin     :: Advice Plus, All-In Plan
- ria-aplus-artesys   :: Advice Plus & Artesys
- ria-pronvest        :: ProNvest
- 403b                :: 403(b)
- zerobal-active      :: Zero Balance Active
- terminated-with-bal :: Terminated with Balance

#### Investment Advice

- sdb        :: Self-Directed Brokerage
- pronvest   :: ProNVest
- adviceplus :: Advice Plus

#### From FT

- a.adam59           :: a.adam59 (2014-03-21)
- a.doo58            :: a.doo58 (2014-03-17)
- c.dang-G31689      :: c.dang (2014-05-29)
- d.louk75           :: d.louk75 (2014-03-24)
- d.louk75-pendingIS :: d.louk75 with pending Interest Sweep (2014-03-13)
- d.zemel            :: d.zemel (2014-03-30)
- c.thom             :: c.thom (2014-03-06)
- j.gray33           :: j.gray33 (2014-03-04)
- j.woot80           :: j.woot80 (2014-03-17)
- m.klin36           :: m.klin36 (2014-02-24)
- n.talb-O74645      :: n.talb (2014-04-21)
- t.alo34-G35781     :: t.alo34 (2014-05-30)
- v.march            :: v.march (2014-03-17)

## Individual

- whole-life                                :: AUL/PML Whole Life (Cyber Life)
- whole-life-suspended                      :: Suspended |
- whole-life-terminated                     :: Terminated
- term                                      :: 20 Year Term (AUL)
- term-sl                                   :: (SL)
- universal-life                            :: Universal Life (AUL)
- universal-life-sl                         :: (SL)
- universal-life-suspended                  :: Suspended (SL) |
- universal-life-terminated                 :: Terminated
- interest-sensitive-life                   :: Interest Sensitive Life (PML)
- fixed-annuity                             :: Fixed Annuity (AUL)
- fixed-annuity-suspended                   :: Suspended |
- fixed-annuity-terminated                  :: Terminated
- fixed-annuity-spfa                        :: Fixed Annuity SPFA (AUL)
- fixed-annuity-spfa-suspended              :: Suspended |
- fixed-annuity-spfa-terminated             :: Terminated
- retirement-annuity                        :: Retirement Annuity (AUL)
- retirement-annuity-suspended              :: Suspended |
- retirement-annuity-terminated             :: Terminated
- pml-health                                :: PML Health
- pml-health-suspended                      :: Suspended |
- pml-health-terminated                     :: Terminated
- variable-ul                               :: United Life Variable
- state_life_asset_care_with_cob            ::  State Life AssetCare with COB
- state_life_asset_care_with_terminated_cob :: Terminated
- state_life_asset_care_without_cob         :: State Life AssetCare without COB
- aul_indexedAnnuity                        :: Indexed Annuity
- aul-indexedAnnuity-suspended              :: Suspended |
- aul-indexedAnnuity-terminated             :: Terminated
- fdia                                      :: Payout Annuity
- statelife_indexedAnnuity                  :: SL Indexed Annuity Care

### From FT

- m.coy36  :: m.coy36
- g.widrig :: g.widrig
