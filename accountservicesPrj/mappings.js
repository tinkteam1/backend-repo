var config = {
    baseUrl: '/secure/rest'
};

module.exports = {
    delete: {
        transactions: {
            route: config.baseUrl + '/account/([^/]+)/pendingTransactions',
            folder: 'transactions'
        }
    },
    get: {
        accountconfig: {
            route: config.baseUrl + '/account/([^/]+)/config',
            folder: 'accountconfig'
        },
        accounts: {
            route: config.baseUrl + '/accounts',
            folder: 'accounts',
            delay: 500
            //,fail: 500
        },
        accounts_balances: {
            route: config.baseUrl + '/accounts/balances',
            folder: 'accounts'
        },
        accountsummary: {
            route: config.baseUrl + '/account/([^/]+)$',
            folder: 'accountsummary',
            delay: 300
            // fail: 500
            //,fail: 423
        },
        advisors: {
            route: config.baseUrl + '/account/([^/]+)/advisors',
            folder: 'advisors',
            delay: 1000
        },
        advisorPlanModels: {
            route: config.baseUrl + '/account/([^/]+)/advisorPlanModels',
            folder: 'advisorPlanModels',
            delay: 1000
        },
        advisorRTQRecommendModel: {
            route: config.baseUrl + '/account/([^/]+)/advisorRTQRecommendModel',
            folder: 'advisorRTQRecommendModel',
            delay: 1000
        },
        advisorRTQ: {
            route: config.baseUrl + '/account/([^/]+)/advisorRTQ',
            folder: 'advisorRTQ',
            delay: 1000
        },
        accountmessages: {
            route: config.baseUrl + '/accountmessages/([^/]+)',
            folder: 'accountmessages',
            delay: 3000
        },
        balance: {
            route: config.baseUrl + '/account/([^/]+)/balance',
            folder: 'balance'
        },
        beneficiary: {
            route: config.baseUrl + '/account/([^/]+)/beneficiaries',
            folder: 'beneficiary'
        },
        change_security_questions: {
            route: '/oasec/secure/changeSecurityInformation.json',
            folder: 'change_security_questions'
        },
        deferral: {
            route: config.baseUrl + '/account/([^/]+)/deferral',
            folder: 'deferral'
        },
        deferralSource: {
            route: config.baseUrl + '/account/([^/]+)/deferralSource',
            folder: 'deferralSource'
        },
        home_office: {
            route: config.baseUrl + '/user/applications',
            folder: 'home_office'
        },
        loan_calculator: {
            route: config.baseUrl + '/account/([^/]+)/loanCalculator/calculate',
            folder: 'loan_calculator'
        },
        loan_history: {
            route: config.baseUrl + '/account/([^/]+)/loanHistory/.*',
            folder: 'loan_history'
        },
        loan_summary: {
            route: config.baseUrl + '/account/([^/]+)/loanSummary',
            folder: 'loan_summary'
        },
        performance: {
            route: config.baseUrl + '/account/([^/]+)/fundPerformance',
            folder: 'performance'
        },
        personal_information: {
            route: config.baseUrl + '/account/([^/]+)/personalData',
            folder: 'personal_information'
        },
        personalTransferStatus: {
            route: config.baseUrl + '/account/([^/]+)/personalTransferStatus',
            folder: 'personalTransferStatus',
            delay: 5000
        },
        plan_summary: {
            route: config.baseUrl + '/account/([^/]+)/planSummary',
            folder: 'plan_summary'
        },
        securityunimpersonate: {
            route: config.baseUrl + '/undoimpersonation',
            folder: 'securityunimpersonate'
        },
        sidenav: {
            route: config.baseUrl + '/fake/sidenav',
            folder: 'nav'
        },
        siteaccess: {
            route: config.baseUrl + '/account/([^/]+)/siteaccess',
            folder: 'accountsiteaccess'
        },
        siteminder_ping: {
            route: '/secure/servlet/BlankImage',
            folder: 'siteminder_ping'
        },
        transactions: {
            route: config.baseUrl + '/account/([^/]+)/pendingTransactions',
            folder: 'transactions'
        },
        transfer_autoRebalance: {
            route: config.baseUrl + '/account/([^/]+)/transfer/autoRebalanceTransfer',
            folder: 'transfer_autoRebalance'
        },
        transfer_changeIOE: {
            route: config.baseUrl + '/account/([^/]+)/investmentOptionElection',
            folder: 'transfer_changeIOE',
            delay: 1000
        },
        transfer_percent: {
            route: config.baseUrl + '/account/([^/]+)/transfer/percentTransfer',
            folder: 'transfer_percent'
        },
        transfer_dollar: {
            route: config.baseUrl + '/account/([^/]+)/transfer/dollarTransfer',
            folder: 'transfer_dollar'
        },
        transfer_dca: {
            route: config.baseUrl + '/account/([^/]+)/transfer/dcaTransfer',
            folder: 'transfer_dca'
        },
        transfer_interestSweep: {
            route: config.baseUrl + '/account/([^/]+)/transfer/interestSweepTransfer',
            folder: 'transfer_interestSweep'
        },
        transfer_oneTimeRebalance: {
            route: config.baseUrl + '/account/([^/]+)/transfer/oneTimeRebalanceTransfer',
            folder: 'transfer_oneTimeRebalance'
        },
        wcm_data: {
            route: '/wcm/wps/wcm/connect/appdesign/as2data/.*',
            folder: 'wcm'
        },
        wcm_acknowledgement_without_prospectus: {
            route: '/wcm/wps/wcm/connect/appcontent/Account-Services/AcknowledgementWithoutProspectus',
            folder: 'wcm_content',
            fileName: 'AcknowledgementWithoutProspectus',
            isHtml: true
        },
        wcm_acknowledgement_with_prospectus: {
            route: '/wcm/wps/wcm/connect/appcontent/Account-Services/AcknowledgementWithProspectus',
            folder: 'wcm_content',
            fileName: 'AcknowledgementWithProspectus',
            isHtml: true
        },
        wcm_additional_information: {
            route: '/wcm/wps/wcm/connect/appcontent/Account-Services/AdditionalInformationAndDeferralAgreement',
            folder: 'wcm_content',
            fileName: 'AdditionalInformationAndDeferralAgreement',
            isHtml: true
        },
        wcm_education_getting_started: {
            route: '/wcm/wps/wcm/connect/oacontent/oneamerica/odit/getting-started',
            folder: 'wcm_education',
            fileName: 'getting-started'
        },
        wcm_education_investing: {
            route: '/wcm/wps/wcm/connect/oacontent/oneamerica/odit/investing',
            folder: 'wcm_education',
            fileName: 'investing'
        },
        wcm_education_nearing_retirement: {
            route: '/wcm/wps/wcm/connect/oacontent/oneamerica/odit/nearing-retirement',
            folder: 'wcm_education',
            fileName: 'nearing-retirement'
        },
        wcm_education_overview: {
            route: '/wcm/wps/wcm/connect/oacontent/oneamerica/odit/overview',
            folder: 'wcm_education',
            fileName: 'overview'
        },
        wcm_education_personal_finance: {
            route: '/wcm/wps/wcm/connect/oacontent/oneamerica/odit/personal-finance',
            folder: 'wcm_education',
            fileName: 'personal-finance'
        },
        wcm_education_resource_center: {
            route: '/wcm/wps/wcm/connect/oacontent/oneamerica/odit/resource-center',
            folder: 'wcm_education',
            fileName: 'resource-center'
        },
        wcm_education_retirement_101: {
            route: '/wcm/wps/wcm/connect/oacontent/oneamerica/odit/retirement-101',
            folder: 'wcm_education',
            fileName: 'retirement-101'
        },
        wcm_state_fraud: {
            route: '/wcm/wps/wcm/connect/appcontent/Account-Services/StateSpecificFraudWarning',
            folder: 'wcm_content',
            fileName: 'StateSpecificFraudWarning',
            isHtml: true
        },
        plan_document: {
            route: config.baseUrl + '/account/([^/]+)/planDocuments',
            folder: 'plan_document'
        }
    },
    post: {
        accountactivity: {
            route: config.baseUrl + '/account/([^/]+)/report/accountactivity',
            folder: 'accountactivity',
            delay: 9000
        },
        advisorModelChange: {
            route: config.baseUrl + '/account/([^/]+)/advisorModelChange',
            folder: 'advisorModelChange'
        },
        advisorServiceDecline: {
            route: config.baseUrl + '/account/([^/]+)/advisorServiceDecline',
            folder: 'advisorServiceDecline'
        },
        advisorServiceEnroll: {
            route: config.baseUrl + '/account/([^/]+)/advisorServiceEnroll',
            folder: 'advisorServiceEnroll'
        },
        advisorServiceTerm: {
            route: config.baseUrl + '/account/([^/]+)/advisorServiceTerm',
            folder: 'advisorServiceTerm'
        },
        beneficiary: {
            route: config.baseUrl + '/account/([^/]+)/beneficiaries',
            folder: 'beneficiary'
        },
        change_userid: {
            route: '/oasec/secure/changeUserId.json',
            folder: 'change_userid'
        },
        change_password: {
            route: '/oasec/secure/changePassword.json',
            folder: 'change_password'
        },
        change_security_questions: {
            route: '/oasec/secure/changeSecurityInformation.json',
            folder: 'change_security_questions'
        },
        contactUsFeedback: {
            route: config.baseUrl + '/account/([^/]+)/contactUsFeedback',
            folder: 'contactUsFeedback'
        },
        contributiondetail: {
            route: config.baseUrl + '/account/([^/]+)/report/contributiondetail',
            folder: 'contributiondetail'
        },
        deferral: {
            route: config.baseUrl + '/account/([^/]+)/deferral',
            folder: 'deferral'
        },
        enrollment: {
            route: config.baseUrl + '/account/([^/]+)/enrollment',
            folder: 'enrollment'
        },
        homeoffice_search: {
            route: config.baseUrl + '/homeoffice/search',
            folder: 'homeoffice_search',
            delay: 1000
        },
        loan_application: {
            route: config.baseUrl + '/account/([^/]+)/loanCalculator/transactionSubmission',
            folder: 'loan_application'
        },
        loan_calculator: {
            route: config.baseUrl + '/account/([^/]+)/loanCalculator/calculate',
            folder: 'loan_calculator'
        },
        loan_projection: {
            route: config.baseUrl + '/account/([^/]+)/loanProjection',
            folder: 'loan_projection'
        },
        pendingTransactionsDetail: {
            route: config.baseUrl + '/account/([^/]+)/pendingTransactionsDetail',
            folder: 'pendingTransactionsDetail',
            delay: 9000,
            fileName: function (req) {
                var name;
                switch (req.body.transCategory) {
                    case 'Transfer':
                    switch (req.body.transactionSubCategory) {
                        case 'AutoRebalance':
                        name = 'auto-rebalance';
                        break;
                        case 'DollarCostAverage':
                        name = 'dollar-cost-average';
                        break;
                        case 'InterestSweep':
                        name = 'interest-sweep';
                        break;
                        case 'Dollar':
                        name = 'dollar';
                        break;
                        case 'Percent':
                        name = 'percent';
                        break;
                    }
                    break;
                    case 'IOE':
                    name = 'ioe';
                    break;
                    case 'Loan':
                    name = 'loan';
                    break;
                    case 'BeneficiaryChange':
                    name = 'beneficiary-change';
                    break;
                    case 'DeferralChange':
                    name = 'deferral-change';
                    break;
                }
                return name;
            }
        },
        recordAdvisorSiteEnroll: {
            route: config.baseUrl + '/account/([^/]+)/recordAdvisorSiteEnroll',
            folder: 'recordAdvisorSiteEnroll'
        },
        transactions: {
            route: config.baseUrl + '/account/([^/]+)/pendingTransactions',
            folder: 'transactions'
        },
        transfer_autoRebalance: {
            route: config.baseUrl + '/account/([^/]+)/transfer/autoRebalanceTransfer',
            folder: 'transfer_autoRebalance'
        },
        transfer_changeIOE: {
            route: config.baseUrl + '/account/([^/]+)/investmentOptionElection',
            folder: 'transfer_changeIOE'
        },
        transfer_dca: {
            route: config.baseUrl + '/account/([^/]+)/transfer/dcaTransfer',
            folder: 'transfer_dca'
        },
        transfer_dollar: {
            route: config.baseUrl + '/account/([^/]+)/transfer/dollarTransfer',
            folder: 'transfer_dollar'
        },
        transfer_interestSweep: {
            route: config.baseUrl + '/account/([^/]+)/transfer/interestSweepTransfer',
            folder: 'transfer_interestSweep'
        },
        transfer_oneTimeRebalance: {
            route: config.baseUrl + '/account/([^/]+)/transfer/oneTimeRebalanceTransfer',
            folder: 'transfer_oneTimeRebalance'
        },
        transfer_percent: {
            route: config.baseUrl + '/account/([^/]+)/transfer/percentTransfer',
            folder: 'transfer_percent'
            //,fail: 400
        },
        unitvaluesbyday: {
            route: config.baseUrl + '/account/([^/]+)/report/unitvalues/byday',
            folder: 'unitvaluesbyday'
        },
        unitvaluesbymonth: {
            route: config.baseUrl + '/account/([^/]+)/report/unitvalues/bymonth',
            folder: 'unitvaluesbymonth',
            delay: 9000
        },
        personal_data: {
            route: config.baseUrl + '/account/([^/]+)/personalData',
            folder: 'personal_information'
        }
    }
};
