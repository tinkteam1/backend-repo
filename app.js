/*global module */
/*jshint node:true */
"use strict";

// Application Dependencies
var dataUtils = require('./dataUtils');
var express   = require('express');
var fs        = require('fs');
var util      = require('util');

// build single list of app route mappings
var routeMappings = require('./routeMappings');

// put an entry for each HTTP verb you will support
var verbs = [ 'delete', 'get', 'post', 'put' ];

// build master list of all routes from the apps
// that are included
var routes = (function() {
    var current;
    var routeType;
    var rts = {};
    var modifiedRoute = {};
    // build master list of each verb routes
    verbs.forEach(function(verb) {
        rts[verb] = [];
    });

    // populate that master list
    Object.keys(routeMappings).forEach(function(namespace) {
        // values in accountServicesPrj, for example
        current = routeMappings[namespace];
        Object.keys(current).forEach(function(item) {
            // values in current.get, for example
            routeType = current[item];
            Object.keys(routeType).forEach(function(route) {
                // for example, the GET routes for accountServicesPrj.get
                // handle it here before we get to express
                // This would be the ideal place to check for collisions
                // we append the namespace the route came from here as well
                modifiedRoute = routeType[route];
                modifiedRoute.namespace = namespace;
                rts[item].push(modifiedRoute);
            });
        });
    });

    return rts;
})();

var allowCORS = function(req, res, next) {
    res.header('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, X-File-Name, Content-Type, Cache-Control');
    res.header('Access-Control-Allow-Method', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Credentials', 'true');
    if( 'OPTIONS' === req.method ) {
        res.header('Content-Type','application/json');
        res.send( 203, JSON.stringify({ msg: 'OK' }));
        res.end();
    } else {
        next();
    }
};

// main express setup
var app = module.exports = express();
app.use(allowCORS);

// AS built a logging service
app.post('/secure/rest/ui/log', function(req, res) {
    util.log("Browser logged: " + req.body.message);
    res.end();
});

// set up route handling in express
Object.keys(routes).forEach(function(routeType) {
    // routes.VERB is now an array
    routes[routeType].forEach(function(routeObj) {
        // app.get('/foo', handler)
        app[routeType](routeObj.route, function(req, res, next) {
            dataUtils.handleXHR.call(this, req, res, next, routeObj);
        });
    });
});

app.listen(5000);
