var prefix = '/employeebenefits';

var config = {
    ebenBaseUrl              : prefix + '/secure/rest',
    oasecBaseUrl             : prefix + '/oasec/secure/rest',
    oasecBasePublicUrl       : prefix + '/oasec/public/rest'
};

function failBenefitAccountsSearch(req, routeObj, persona) {

        return {

            // Service level error
            // Should show a general error message on the site
            // code: 500,

            // No error message, means the user is not logged in
            code: 401,

            // Filename will not matter either way
            fileName: 'fail'
        };
    return false;
}

// Fail the user route when a public persona is used
// i.e., when there is not a 'logged in' user
function failGetUser(req, routeObj, persona) {

    if (/^public/.test(persona) === true) {
        return {

            // Service level error
            // Should show a general error message on the site
            // code: 500,

            // No error message, means the user is not logged in
            code: 401,

            // Filename will not matter either way
            fileName: 'fail'
        };
    }
    return false;
}

// Return back a failure for the forgot user id service
function failPostForgotUserId(req, routeObj, persona) {

    return {

        // Service error
        // code: 500,
        // fileName: 'fail-forgotuserid-service-error'

        // Captcha
        code: 400,
        fileName: 'fail-forgotuserid'

        // Email address not found error
        // With this service, even when there is
        // an error, we bring back a 200.
        // We do not want to allow email sniffing by exposing
        // what addresses are valid. So even when this fails, we return
        // 200 and just smile and nod.
        // code: 200,
        // fileName: 'fail-forgotuserid'

    };
}


// This route just brings back the info for the user to fill out
// So the only possible error is server side, no validation on a GET
// Should show a generic error message
function failGetSecurityInfo(req, routeObj, persona) {
    return {

        // General error
        code: 500,
        fileName: 'fail-securityinfo-service-error'

    };
}

// First step of Forgot Password flow
function failPostForgotPasswordUserId(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'post' method
        // will be prepended to the filename

        // General error
        // code: 500,
        // fileName: 'fail-forgotpassword-userid-service-error'

        // Field level error
        code: 400,
        fileName: 'fail-forgotpassword-userid'
    };
}

function failPostForgotPasswordSecurityItems(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'post' method
        // will be prepended to the filename

        // General error
        // code: 500,
        // fileName: 'fail-forgotpassword-securityitems-service-error'

        // Field level error
        code: 400,
        fileName: 'fail-forgotpassword-securityitems'
    };
}

function failPostChangePassword(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'post' method
        // will be prepended to the filename

        // General error
        code: 500,
        fileName: 'fail-changepassword-service-error'

        // Field level error
        // code: 400,
        // fileName: 'fail-changepassword'
    };
}

function failPostChangeUserId(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'post' method
        // will be prepended to the filename

        // General error
        // code: 500,
        // fileName: 'fail-changeuserid-service-error'

        // Field level error
        code: 400,
        fileName: 'fail-changeuserid'
    };
}

function failPostForgotPasswordNewPassword(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'post' method
        // will be prepended to the filename

        // General error
        // code: 500,
        // fileName: 'fail-forgotpassword-newpassword-service-error'

        // Field level error
        code: 400,
        fileName: 'fail-forgotpassword-newpassword'
    };
}

function failPostSecurityInfo(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'post' method
        // will be prepended to the filename

        // General error
        code: 500,
        fileName: 'fail-securityinfo-service-error'

        // Field level error
        // code: 400,
        // fileName: 'fail-securityinfo'
    };
}

function failPolicyholders(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'put' method
        // will be prepended to the filename

        // General error
        code: 500,
        fileName: 'fail-service-error'

        // Field level error
        //code: 400,
        //fileName: 'fail-policyholders'
    };
}

function failEmployeeCoverages(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'put' method
        // will be prepended to the filename

        // General error
        //code: 500,
        //fileName: 'fail-service-error'

        // Field level error
        code: 400,
        fileName: 'fail-employee-coverages'
    };
}

function failEmployees(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'put' method
        // will be prepended to the filename

        // General error
        //code: 500,
        //fileName: 'fail-service-error'

        // Field level error
        //code: 400,
        //fileName: 'fail-employeeupdate-employees'
		
		//Siteminder timeout error
		code: 401,
		fileName: 'fail'
    };
}

function failGetTermReasons(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'put' method
        // will be prepended to the filename

        // General error
        //code: 500,
        //fileName: 'fail-service-error'

        // Field level error
        code: 400,
        fileName: 'fail-termreasons'
    };
}

function failPut(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'put' method
        // will be prepended to the filename

        // General error
        //code: 500,
        //fileName: 'fail-service-error'

        // Field level error
        //code: 400,
        //fileName: 'fail'
		
		//Siteminder timeout error
		code: 401,
		fileName: 'fail'
    };
}

function failPutCoverages(req, routeObj, persona) {
    return {
        // Note that in both these cases, the 'put' method
        // will be prepended to the filename

        // General error
        //code: 500,
        //fileName: 'fail-service-error'

        // Field level error
        code: 400,
        fileName: 'fail-coverages'
    };
}


module.exports = {
    get: {
        benefitAccounts: {
            folder         : 'benefits',
            fileName       : 'benefit-accounts',
            generateStatus : true,
            route          : new RegExp(config.ebenBaseUrl + '/benefits/accounts$')
        },
        benefitAccountsSearch: {
			//fail           : failBenefitAccountsSearch,
            folder         : 'benefits',
            fileName       : 'benefit-accounts-search',
            generateStatus : true,
            route          : new RegExp(config.ebenBaseUrl + '/benefits/accounts/search$')
        },
        benefitDocuments: {
            folder         : 'benefits',
            fileName       : 'benefit-documents',
            generateStatus : true,
            route          : new RegExp(config.ebenBaseUrl + '/benefits/accounts.*/documents.*$')
        },
        delegates: {
            folder         : 'delegates',
            // Currently there is only a single producer who has delegates
            // Add more producers in both the users folder and the delegates folder
            // as needed. The filename in each case should be the value set in the
            // persona cookie.
            // Example - if the persona cookie is msi.benefits, then the user file
            // is expected to be users/msi.benefits.json and the delegates file is
            // expected to be delegates/msi.benefits.json
            generateStatus : true,
            route          : new RegExp(config.ebenBaseUrl + '/delegates$')
        },
	    delegatePolicies: {
	         folder         : 'delegates',
             fileName       : 'delegate-policyholders',
             generateStatus : true,
             route          : new RegExp(config.ebenBaseUrl + '/delegatePolicies$')
	    },
		employeeCoverages: {
			//fail           : failEmployeeCoverages,
            folder         : 'employeeupdate',
            fileName       : 'employee-coverages',
            generateStatus : true,
            route          : new RegExp(config.ebenBaseUrl + '/employees.*/coverages$')
        },
		eoi: {
			folder         : 'eoi',
			fileName       : 'vendor-list',
			generateStatus : true,
			route          : new RegExp(config.ebenBaseUrl + '/eoi/vendors$')
		},
		policyholders: {
            // Uncomment and edit above to test error handling
            //fail           : failPolicyholders,		
            folder         : 'employeeupdate',
            fileName       : 'policyholders',
            generateStatus : false,
            route          : new RegExp(config.ebenBaseUrl + '/policyholders$')
        },
        securityInfo: {
            // This is the GET made to pull back existing information
            // Uncomment and edit above to test error handling
            // fail           : failGetSecurityInfo,
            folder         : 'oasec',
            fileName       : 'securityinfo',
            generateStatus : true,
            route          : new RegExp(config.oasecBaseUrl + '/securityInfo/questions$')
        },
		termReasons: {
			//fail           : failGetTermReasons,
            folder         : 'employeeupdate',
            fileName       : 'term-reasons',
            generateStatus : true,
            route          : new RegExp(config.ebenBaseUrl + '/valueDescriptionList$')
        },
        user: {
            // Uncomment and edit above to test error handling
            // fail           : failGetUser,
            folder         : 'user',
            generateStatus : true,
            // allows us to test the public role
            route          : new RegExp(config.ebenBaseUrl + '/user$')
        }
    },

    post: {
        forgotPasswordNewPassword: {
            // Uncomment and edit above to test error handling
            // fail           : failPostForgotPasswordNewPassword,
            folder         : 'oasec',
            fileName       : 'forgotpassword-newpassword-success',
            generateStatus : false,
            noCookie       : true,
            route          : new RegExp(config.oasecBasePublicUrl + '/forgotPassword/password$')
        },
        forgotPasswordSecurityItems: {
            // Uncomment and edit above to test error handling
            // fail           : failPostForgotPasswordSecurityItems,
            folder         : 'oasec',
            fileName       : 'forgotpassword-securityitems-success',
            generateStatus : false,
            noCookie       : true,
            route          : new RegExp(config.oasecBasePublicUrl + '/forgotPassword/answers$')
        },
        forgotPasswordUserId: {
            // Uncomment and edit above to test error handling
            // fail           : failPostForgotPasswordUserId,
            folder         : 'oasec',
            fileName       : 'forgotpassword-userid-success',
            generateStatus : false,
            noCookie       : true,
            route          : new RegExp(config.oasecBasePublicUrl + '/forgotPassword/questions$')
        },
        changePassword: {
            // Uncomment and edit above to test error handling
            // fail           : failPostChangePassword,
            folder         : 'oasec',
            fileName       : 'changepassword-success',
            generateStatus : false,
            route          : new RegExp(config.oasecBaseUrl + '/password')
        },
        changeUserId: {
            // Uncomment and edit above to test error handling
            // fail           : failPostChangeUserId,
            folder         : 'oasec',
            fileName       : 'changeuserid-success',
            generateStatus : false,
            route          : new RegExp(config.oasecBaseUrl + '/userId$')
        },
        employees: {
			// Uncomment and edit above to test error handling
            //fail           : failEmployees,
            folder         : 'employeeupdate',
            fileName       : 'employeeupdate-employees',
            generateStatus : false,
            route          : new RegExp(config.ebenBaseUrl + '/employees$')
        },
        forgotUserId: {
            // Uncomment and edit above to test error handling
            // fail           : failPostForgotUserId,
            folder         : 'oasec',
            fileName       : 'forgotuserid',
            generateStatus : false,
            noCookie       : true,
            route          : new RegExp(config.oasecBasePublicUrl + '/forgotUserId')
        },
		policyholderEmployees: {
			//fail           : failEmployees,
            folder         : 'employeeupdate',
            fileName       : 'policyholder-employees',
            generateStatus : false,
            route          : new RegExp(config.ebenBaseUrl + '/policyholders.*/employees')
        },
        securityinfo: {
            // Uncomment and edit above to test error handling
            // fail           : failPostSecurityInfo,
            folder         : 'oasec',
            fileName       : 'securityinfo',
            generateStatus : false,
            route          : new RegExp(config.oasecBaseUrl + '/securityInfo/answers$')
        }
    },
	
	put: {
		updateCoverages: {
			//fail: failPutCoverages,
			folder: 'employeeupdate',
            route: new RegExp(config.ebenBaseUrl + '/employees/coverages$')
		},
		updateEmployee: {
			//fail: failPut,
			folder: 'employeeupdate',
            route: new RegExp(config.ebenBaseUrl + '/employees.*/')
		}
	}


};
