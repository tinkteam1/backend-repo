## Files in this directory

## all-securityinfo.json
Returned on a GET on the service.

## fail-securityinfo.json
Returned if you turn on fail and specify the file. Shows field level errors. Doesn't really make sense for the GET, since that just brings back the empty fields for the UI to render.

## fail-securityinfo-service-general.json
Returned if you turn on fail and specify the file. Shows general errors.

## post-all-securityinfo.json
Returned on a successful POST to the service (change info succeeds)

## post-fail-securityinfo.json
Returned on a failed POST to the service (change info failed)

## post-fail-securityinfo-service-general.json
Returned on a failed POST to the service, with a general error

## GET  on the service
Returns:

`
{
        "items": [
            {
                "securityQuestions": [
                    {
                        "id": "mothersMaidenName",
                        "question": "Mother's Maiden Name",
                        "answer": ""
                    },
                    {
                        "id": "birthState",
                        "question": "Birth State",
                        "answer": ""
                    },
                    {
                        "id": "KeywordPhrase",
                        "question": "Forgotten Password Question",
                        "answer": ""
                    },
                    {
                        "id": "Keyword",
                        "question": "Your Answer",
                        "answer": ""
                    }
                ],
                "emailAddress": "blah@blah.com",
                "confirmEmailAddress": "blah@blah.com"
            }
        ],
        "count": 1,
        "errors": [],
        "errorCount": 0
    }
    }
    `

## POST

Requires this data to be sent:
`
{
    "securityQuestions": [
        {
            "id": "mothersMaidenName",
            "question": "Mother's Maiden Name",
            "answer": "SMITH2"
        },
        {
            "id": "birthState",
            "question": "Birth State",
            "answer": "IN2"
        },
        {
            "id": "KeywordPhrase",
            "question": "Forgotten Password Question",
            "answer": "FAVORITE COLOR2"
        },
        {
            "id": "Keyword",
            "question": "Your Answer",
            "answer": "PURPLE2"
        }
    ],
    "emailAddress": "blah2@blah.com",
    "confirmEmailAddress": "blah2@blah.com"
}
`

## Errors

POST validation error returns list of errors:
`
{
    "OAResponse": {
        "items": null,
        "count": 0,
        "errors": [
            {
                "field": "mothersMaidenName",
                "code": "Mother's Maiden Name is required."
            },
            {
                "field": "birthState",
                "code": "Birth State is required."
            }
        ],
        "errorCount": 2
    }
}
`
