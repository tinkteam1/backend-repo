/* global describe, expect, it */

define([
    'intern!bdd',
    'intern/chai!expect',
    'intern/dojo/node!../../../dataUtils'
], function (
    bdd,
    expect,
    dataUtils
) {

    /* jshint withstmt: true */
    with (bdd) {
    /* jshint withstmt: false */
        describe('A suite of sample tests', function() {

            describe('Two simple unit tests on equality', function() {

                it('should show that true is true', function() {
                    expect(true).to.equal(true);
                });

                it('should show that false is not true', function() {
                    expect(false).to.not.equal(true);
                });

            });

            describe('A single unit test suite', function() {

                it('should show that 1 is not equal to 0', function() {
                    expect(1).to.not.equal(0);
                });

            });

        });

    }

});
