// Configuration for running unit tests with intern
// These can only be tests that do NOT take a browser

define([
    './intern-base-config'
], function(
    baseConfig
) {

    // This is nulled out in order to NOT overwrite existing intern
    // mappings that are used internally by intern
    // https://github.com/theintern/intern/issues/348
    baseConfig.loader.map = null;

	baseConfig.suites = [
        // define the specs you want to run in the file below
        'test/unit/specs/example-unittest-spec'
    ];

    return baseConfig;
});
