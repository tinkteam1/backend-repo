// Base setup for intern.
// Override settings in each config file as needed

// See https://github.com/theintern/intern/wiki/Configuring-Intern
// for more about these options

define({

    capabilities: {},

    environments: [
        // Lists the
        // default to chrome because we have the
        // webdriver in our repo, and we can pass
        // a direct command line argument to it if
        // needed.
        // PhantomJS has issues with functional tests
        {
            // browserName : 'chrome'
            browserName : 'phantomjs'
            // Can define these as well, but for now
            // we use using whatever is installed
            // version     : [ '23', '24' ],
            // platform    : [ 'Linux', 'Mac OS 10.8' ]
        }
    ],

    // excludes from code coverage
    excludeInstrumentation: (function() {
        var paths = [
            'bin',
            'dataContract',
            'documentation',
            'grunt',
            'gulp',
            'node_modules',
            'saucelabs',
            'scripts',
            'test/config',
            'test/dist',
            'test/functional',
            'specs',
            'lib',
            'untouched'
        ].join('|');
        return new RegExp(paths);
    })(),

    // empty here since each config file may or may not want
    // functional testing
    functionalSuites: [],

    // set to true
    // for debugging or to view test results
    // must change browser above to chome as well
    // if not done already
    leaveRemoteOpen: false,

    // base AMD setup for common modules
    // WARNING
    // https://github.com/theintern/intern/issues/348
    // if you try to mix a local loader.map config
    // with the provided intern mappings like `intern/dojo/node!`
    // things will blow up
    loader: {
        map: {
            '*': {
                'chai'       : '../../../node_modules/chai/chai'
            }
        }
    },

    // How many environments to run at once
    // If using something that's hosted or constrained, lower
    maxConcurrency: Infinity,

    // The port on which the instrumenting proxy will listen
    proxyPort: 9000,

    // A fully qualified URL to the Intern proxy
    proxyUrl: 'http://localhost:9000/',

    // each config file adds different reporters to this
    // cobertura is used by Jenkins
    reporters: [
        // 'cobertura',
        'combined',
        'console'
    ],

    // unit test suites
    // empty here since each config file may or may not want
    // unti testing
    suites: [],

    tunnel: 'NullTunnel'

});
