// Specs should be written in the 'object' format
// provided by intern. See an example here:
// https://github.com/theintern/intern/wiki/Writing-Tests-with-Intern#functional

// CommonJS style modules need this prefix
// when included below in the define
// 'intern/dojo/node!../modules/modulename'

// For a more comprehensive example of testing, see
// https://bitbucket.org/ebus/oneamerica/pull-request/21/automate-running-the-functional-tests

define([
    'intern!object',
    'intern/chai!expect',
    'intern/dojo/node!leadfoot/keys',
    'intern/dojo/node!leadfoot/command',
    'intern/dojo/node!../functional-utils',
    'intern/dojo/node!../helpers/example-helper',
    // env config
    'intern/dojo/node!../../../gulp/config/index',
    // common css selectors
    'intern/dojo/node!../../../src/js/config/selectors'
], function (
    registerSuite,
    expect,
    KEYS,
    Command,
    testUtils,
    exampleHelper,
    CONFIG,
    SEL
) {

    // page to test
    var page = 'index.html';

    registerSuite({

        name: 'Initial Page Load',

        'Test Initial Page Load': function () {

            //////////////////////////////////////////////////
            // BEGIN BOILERPLATE REQUIRED IN EVERY TEST
            //////////////////////////////////////////////////
            var remote = testUtils.initTest.call(this, page);
            //////////////////////////////////////////////////
            // END BOILERPLATE REQUIRED IN EVERY TEST
            //////////////////////////////////////////////////

            // Message that will show if it does not pass
            var msg = 'The page should have 2 H1s present';

            return remote.findAllByCssSelector('h1')
                         .then(function (headers) {
                             expect(headers.length, msg).to.equal(2);
                         });
        },

        'Test Initial Page Load With some Examples of Selenium Functions': function () {

            //////////////////////////////////////////////////
            // BEGIN BOILERPLATE REQUIRED IN EVERY TEST
            //////////////////////////////////////////////////
            var remote = testUtils.initTest.call(this, page);
            //////////////////////////////////////////////////
            // END BOILERPLATE REQUIRED IN EVERY TEST
            //////////////////////////////////////////////////

            // Message that will show if it does not pass
            var msg = 'The page should have 2 H1s present';

            return remote.findAllByCssSelector('h1')
                         .end()
                         .sleep(500)
                         .findAllByCssSelector('h1')
                         .then(function (headers) {
                             expect(headers.length, msg).to.equal(2);
                         });
        }

    });

});
