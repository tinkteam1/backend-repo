/* globals registerSuite require */

// MODULES
var CONFIG = require('../../gulp/config/index');
var fs     = require('fs');
var path   = require("path");

var utils = {

    // Does the common setup work.
    // MUST be invoked via call
    // because it needs the scope chain of the actual test.
    initTest: function(page) {

        var url =  [
            'http://',
            CONFIG.DEV_HOST,
            ':',
            CONFIG.DEV_PORT_START,
            '/',
            page
        ].join('');

        if (this.remote.environmentType.webStorageEnabled === true) {
            this.remote
                .clearSessionStorage()
                .clearLocalStorage();
        }

        return this.remote
                   .get(url)
                   .sleep(1000)
                   .setWindowSize(1024, 768)
                   .clearCookies()
                   .end()
                   .sleep(3000);
    },

    // return a selector like this:
    // selector[for='target']
    cssAttribute: function(selector, attr, target) {
        selector = selector || '';
        target = target.substr(1);
        return selector + '[' + attr + '="' + target + '"]';
    }

};

module.exports = utils;
