// Return back the correct WCM data.
function getWcmId(req, routeObj, persona) {

    // These ids must match the ids in the actual repo
    var wcmIds = {
        // REAL WCM IDS BELOW HERE

        // EBen content
        '2cb71c55-6292-4ec2-afbd-9fa1f924c9cd': 'EBEN-admin-of-benefits',
        '871138cc-5cf7-4e33-947c-e8f632cff6e5': 'EBEN-billing-payments',
        '1bbbbaba-51c6-4aa2-99be-5f53de278855': 'EBEN-contact-us',
        'cde26d22-4867-4c14-865a-278a7120da47': 'EBEN-continuing-education',
        '4d7aa3ce-464e-4e19-872e-78ba6ad2b306': 'EBEN-disability',
        'eee17e97-0c7f-44e9-9dbb-81fd16ccb61c': 'EBEN-disability-claims',
        '53fd9852-9edb-43ff-8a9e-06f95453c20a': 'EBEN-eap',
        'ca70463b-99ec-4c09-b321-9852d62ecf4b': 'EBEN-fmla-admin',
        '4bda2b44-8e53-4bd6-8446-d22006a597ed': 'EBEN-forms',
        'bce7b26c-5c72-40bf-a431-92516d94d29a': 'EBEN-home',
        '4a9fd932-cb04-418b-b240-7fa188d34654': 'EBEN-life',
        'bcb15e1b-e992-4cba-9a76-bfde03371b8e': 'EBEN-life-claims',
        '23100bfb-69d7-4c14-ab3a-5d0ce2a6e576': 'EBEN-login',
        '782901b0-f192-40bd-8feb-2addf2728bf5': 'EBEN-online-benefits-admin',
        '0877b520-1fbd-42b3-92b2-b52f2ab46c03': 'EBEN-products-services-overview',
        '25cadcef-405e-44e9-97f9-cb8b4fadface': 'EBEN-resources',
        'aba26c88-024e-4c5d-8544-5957afb2cec1': 'EBEN-social-media',
        '73f73b5c-547b-4529-8ba2-66dffd17615f': 'EBEN-tax-services',
        '45690100-8f5e-4758-abff-c7b3fcb59af5': 'EBEN-tools',
        '8a067c4c-6bc4-4c7a-85ce-98cc13f23b4b': 'EBEN-traditional-long-term-disability',
        '8bb14f02-4553-4c9d-9b1f-b549e072f13a': 'EBEN-traditional-short-term-disability',
        '80d7b3c5-526d-4173-b5eb-aff4a8b6a882': 'EBEN-traditional-term-life-and-add',
        'f0f76e3b-6d64-45e2-82f9-1be373013fda': 'EBEN-training',
        'afc28e2c-cc72-4a65-b090-a1759aab0634': 'EBEN-travel-assistance',
        '3fc8bd60-c3a6-4495-93bb-7fdd322d305b': 'EBEN-voluntary-lump-sum-disability',
        '767aba9e-6ba4-479a-af66-3b418d19e23c': 'EBEN-voluntary-term-life-and-add',
        '6ef2da42-5002-429f-aff5-9f31ed9b6a07': 'EBEN-voluntary-whole-life',
        'f168ee35-ed68-47ba-960c-9b95090cd03e': 'EBEN-voluntary-worksite-disability',

        //OSO content
        '84f733004c8060c78d69ed910725b403'    : 'OSO-selling-concepts-to-grow-your-practice',
        '526e5e15-65da-4e72-a8b6-fec3c1347acf': 'OSO-ELEVATE'

    };

    var guid = req.url.match(routeObj.route, 'g');
    if (guid.length) {
        return (wcmIds[guid[1]] ? wcmIds[guid[1]] : '');
    }

    return '';
}

/**
* get ConnectServlet HTML content file name
*
*    Expected URL format wps/wcm/connect/ind/<end path to content> to better mapping
*    End path will be consider as reference to local file name to serve
*    eg : 
*         if URL is => 'wps/wcm/connect/ind/OnlineServices/Marketing/ELEVATE'
*         Output will be => 'OSO-elevate'
*
*         if URL is => wps/wcm/connect/ind/OnlineServices/Sales+Support/
*                      Selling+Concepts+to+Grow+Your+Practice
*                      /Selling+Concepts+to+Grow+Your+Practice
*
*         Output will be => 'OSO-selling-concepts-to-grow-your-practice'
*
*     Finally file name will be prepended with 'all-'.
*     So expected file name will be 'all-OSO-selling-concepts-to-grow-your-practice' 
*     with extension '.html'
*
**/
function getWcmHTMLFileName (req, routeObj, persona) {
    var wcmPath = req.url.match(routeObj.route, 'g');
    var pathSplitUp = wcmPath[1].split('/');

    if (wcmPath.length && pathSplitUp.length) {
        return pathSplitUp.pop().toLowerCase().replace(/\+| /g, '-');
    }
    return '';
}

/**
* get folderName to load WCM content
*
* For this app need set a cookie (appName), in order to lookup 
* for app specific content.
*   
*  eg:  if appName name is 'oso', 
*       method will return 'wcm_content/oso' as content folder
*
*       Otherwise it will retrun 'wcm_content/' if there is no cookie set
* 
**/
function getWcmHTMLFolderName (req, appName) {
    var wcmFolderName = 'wcm_content/';

    //if appName set add that with wcmFolderName
    if (appName) {
        return wcmFolderName+appName.toLowerCase();    
    };

    return wcmFolderName;
}

// Binary media can be retrieved as well
function getWcmBinary(req, routeObj, persona) {
    var url = req.url.split('?')[0];
    var img = url.match(routeObj.route, 'gim');
    if (img && img.length) {
        return img[1];
    }

    return '';
}

module.exports = {
    get: {
        faq: {
            folder         : 'wcm_content',
            fileName       : 'EBEN-faq-data',
            generateStatus : false,
            noCookie       : true,
            route          : /\/wps\/wcm\/connect\/eben\/eben2data\/listFaqs.*$/
        },
        img: {
            isBinary       : true,
            folder         : 'wcm_content',
            fileName       : getWcmBinary,
            generateStatus : false,
            noCookie       : true,
            route          : /\/wps\/wcm\/connect\/.*\/(.*\.(?:jpe?g|gif|png|mpe?g|mov))$/
        },
        wcm: {
            folder         : 'wcm_content',
            fileName       : getWcmId,
            generateStatus : false,
            noCookie       : true,
            route          : /\/wps\/contenthandler\/wcmrest\/Content\/([^?]+)(?:\?.*)?$/
        },
        wcmConnect: {
            folder         : getWcmHTMLFolderName,
            fileName       : getWcmHTMLFileName,
            isHtml         : true,
            generateStatus : false,
            noCookie       : true,
            route          : /\/wps\/wcm\/connect\/indcontent\/OSO\/([^?]+)(?:\?.*)?$/
        }
    }
};


